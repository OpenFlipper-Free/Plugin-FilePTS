//================================================================
//
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/




//================================================================
//
//  CLASS FilePTSPlugin
//
//    This class is the base class for loading and saving (reading/writing) SplatCloud objects from/to disc.
//
//================================================================

#pragma once


//== INCLUDES ====================================================


#include <QObject>

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/FileInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>
#include <OpenFlipper/BasePlugin/RPCInterface.hh>
#include <OpenFlipper/BasePlugin/AboutInfoInterface.hh>

#include <OpenFlipper/common/Types.hh>

#include <ObjectTypes/SplatCloud/SplatCloud.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>

#include <QComboBox>
#include <QCheckBox>
#include <QLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QPushButton>

#include "ui_ptsLoadWidget.h"

//== CLASS DEFINITION ============================================

class AdaptorBase {

public:
    virtual void clear() = 0;
    virtual void add_point(ACG::Vec3d  _point) = 0;
    virtual void setNormal(ACG::Vec3d _normal) = 0;
    virtual void setColor(ACG::Vec4f  _color) = 0;
    virtual void setColor(ACG::Vec3uc  _color) = 0;
    virtual void setPointSize(float _size ) = 0;
    virtual void setIndex(int _index ) = 0;

    virtual void request_vertex_normals() = 0;
    virtual void request_vertex_colors() = 0;
    virtual void request_point_sizes() = 0;
    virtual void request_indices() = 0;

    virtual void reserve(size_t _size) = 0;

    virtual DataType adaptorType() = 0;
};

class ptsLoadWiget : public QWidget, public Ui::ptsLoadWidget
{
  Q_OBJECT

  public:
    explicit ptsLoadWiget(QWidget *parent = 0) : QWidget(parent) {     setupUi(this);  };
};

/// Enum defining read Order
enum ReadObject{ PointPos,PointNormal,PointColor,PointSize,PointIndex };

class FilePTSPlugin : public QObject, BaseInterface, FileInterface, LoadSaveInterface, LoggingInterface, ScriptInterface, RPCInterface, AboutInfoInterface
{
  Q_OBJECT
  Q_INTERFACES( FileInterface      )
  Q_INTERFACES( LoadSaveInterface  )
  Q_INTERFACES( LoggingInterface   )
  Q_INTERFACES( BaseInterface      )
  Q_INTERFACES( ScriptInterface    )
  Q_INTERFACES( RPCInterface       )
  Q_INTERFACES( AboutInfoInterface )

  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-FilePTS")

signals:

  // -- File Interface --
  void openedFile( int _objectId );

  // -- LoadSave Interface --
  void addEmptyObject( DataType _type, int &_objectId );
  void deleteObject ( int _objectId );
  void updatedObject( int _objectId, const UpdateType &_type );

  //-- Logging Interface --
  void log(                QString _message );
  void log( Logtype _type, QString _message );

  //-- AboutInfoInterface --
  void addAboutInfo(QString _text, QString _tabName);

private slots:

  // -- Base Interface --
  void initializePlugin();
  void noguiSupported() { }

public:

  // standard constructor/destructor
  FilePTSPlugin();
  ~FilePTSPlugin() { }

  //-- Base Interface --
  QString name()         { return QString(    "FilePTS"                            ); }
  QString description( ) { return QString( tr("Load/Save SplatCloud format files") ); }

  // -- File Interface --
  DataType supportedType() { return DATA_SPLATCLOUD; }

  // -- File Interface --
  QString getSaveFilters() { return QString( tr("SplatCloud format files ( *.pts *.bin )") ); }
  QString getLoadFilters() { return QString( tr("SplatCloud format files ( *.pts *.bin )") ); }
  QWidget *saveOptionsWidget( QString /*_currentFilter*/ );
  QWidget *loadOptionsWidget( QString /*_currentFilter*/ );

public slots:

  // -- Base Interface --
  QString version() { return QString( "1.0" ); }

  // -- File Interface --
  int  loadObject(                QString _filename );
  bool saveObject( int _objectId, QString _filename );

private:

  // read binary/text file from disc to scenegraph node
  bool readBinaryFile( const char *_filename, SplatCloud &_splatCloud ) /*const*/;

  bool readTextFile  ( const char *_filename, AdaptorBase& _adaptor );

  // write binary/text file from scenegraph node to disc
  bool writeBinaryFile( const char *_filename, const SplatCloudNode *_splatCloudNode ) /*const*/;
  bool writeTextFile  ( const char *_filename, const SplatCloudNode *_splatCloudNode ) /*const*/;

  // read and decompress a binary chunk from file stream (snappy compression)
  bool readCompressedBinaryChunk( FILE* _file, size_t _compressedSize, char* _dst );

  // Get a read Object Order from ui
  std::vector<ReadObject> getReadObjectOrder();

  // widgets
  ptsLoadWiget *loadOptions_;
  QWidget *saveOptions_;

  // options in the saving menu
  QCheckBox *saveBinaryFile_;
  QCheckBox *saveNormals_;
  QCheckBox *savePointsizes_;
  QCheckBox *saveColors_;
  QComboBox *saveColorRange_;
  QCheckBox *saveIndices_;

  // buttons
  QPushButton *saveMakeDefaultButton_;

private slots:

  // slots called when the Load/Save colors checkbox or binaryfile checkbox was clicked
  void slotUpdateSaveColorRange();

  // slots called when user wants to save the given Load/Save Options as default
  void slotLoadMakeDefaultButtonClicked();
  void slotSaveMakeDefaultButtonClicked();
};


//================================================================
