//================================================================
//
/*===========================================================================*\
*                                                                            *
 *                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                            *
 \*===========================================================================*/


//== INCLUDES ====================================================


#include "FilePTS.hh"

#include "Snappy/snappy.h"



#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <OpenFlipper/common/GlobalOptions.hh>

#include <QTextStream>


//== CONSTANTS ===================================================


// constants of color range drop down box
static const int COLORRANGE_0_1   = 0;
//static const int COLORRANGE_0_255 = 1;


//== IMPLEMENTATION ==============================================

template <typename MeshT>
class AdaptorMesh : public AdaptorBase {
public:
    AdaptorMesh(MeshT& _mesh, DataType _type) : mesh_(_mesh),currentPoint_(0),type_(_type) {}

    virtual ~AdaptorMesh() {

    }

    void clear() override {
        mesh_.clear();
    }

    void add_point(ACG::Vec3d  _point) override {
        currentPoint_ = mesh_.add_vertex(_point);
    }

    void setNormal(ACG::Vec3d _normal) override{
        mesh_.set_normal( currentPoint_,_normal  );
    }

    void setColor(ACG::Vec4f  _color) override{
        mesh_.set_color( currentPoint_,_color  );
    }

    void setColor(ACG::Vec3uc  _color) override{
        mesh_.set_color( currentPoint_, OpenMesh::color_cast <typename MeshT::Color>(_color));
    }

    void setPointSize(float /*_size*/ ) override {

    }

    void setIndex(int /*_size*/ ) override {

    }

    void request_vertex_normals() override {
        mesh_.request_vertex_normals();
    }

    void request_vertex_colors() override {
        mesh_.request_vertex_colors();
    }

    virtual void request_point_sizes() override {

    }

    virtual void request_indices() override {

    }

    void reserve(size_t _size) override {
       mesh_.reserve(_size,0,0);
    }

    virtual DataType adaptorType() override {
        return type_;
    }

private :
    MeshT&                       mesh_;
    typename MeshT::VertexHandle currentPoint_;
    DataType                     type_;
};




class AdaptorSplat : public AdaptorBase {
public:
    AdaptorSplat(SplatCloud& _cloud) : cloud_(_cloud),splatIdx_(-1) {
        cloud_.requestPositions();
    }

    virtual ~AdaptorSplat() {

    }

    void clear() override {
        cloud_.clear();
        cloud_.requestPositions();
    }

    void add_point(ACG::Vec3d  _point) override {
        ++splatIdx_;
        cloud_.pushbackSplat();
        cloud_.positions( splatIdx_ ) = _point;
    }

    void setNormal(ACG::Vec3d _normal) override{
        cloud_.normals( splatIdx_ ) = _normal;
    }

    void setColor(ACG::Vec4f  _color) override{
        cloud_.colors( splatIdx_ ) = OpenMesh::color_cast<SplatCloud::Color>(_color);
    }

    void setColor(ACG::Vec3uc  _color) override{
        cloud_.colors( splatIdx_) = _color;
    }

    void setPointSize(float _size ) override {
        cloud_.pointsizes(splatIdx_) = _size;
    }

    void setIndex(int _index ) override {
        cloud_.indices(splatIdx_) = _index;
    }

    void request_vertex_normals() override {
        cloud_.requestNormals();
    }

    void request_vertex_colors() override {
        cloud_.requestColors();
    }

    virtual void request_point_sizes() override {
        cloud_.requestPointsizes();
    }

    virtual void request_indices() override {
        cloud_.requestIndices();
    }

    void reserve(size_t /*_size*/ ) override {
    }

    virtual DataType adaptorType() override {
        return DATA_SPLATCLOUD;
    }

private :
    SplatCloud&     cloud_;
    int             splatIdx_;
};



FilePTSPlugin::FilePTSPlugin() :
  loadOptions_( nullptr ),
  saveOptions_( nullptr ),
  saveBinaryFile_( nullptr ),
  saveNormals_   ( nullptr ),
  savePointsizes_( nullptr ),
  saveColors_    ( nullptr ),
  saveColorRange_( nullptr ),
  saveIndices_   ( nullptr ),
  saveMakeDefaultButton_( nullptr )
{ }

//----------------------------------------------------------------

void FilePTSPlugin::initializePlugin()
{
  QString info = 
    "This plugin is based on the Snappy compression library by google<br>   "
    "<br>                                                                                    "
    "The following license applies to their code: <br>                                       "
    "Copyright 2005 Google Inc.All Rights Reserved.                         <br>"
    "                                                                       <br>"
    "Redistribution and use in source and binary forms, with or without     <br>"
    "modification, are permitted provided that the following conditions are <br>"
    "met :                                                                  <br>"
    "                                                                       <br>"
    "    *Redistributions of source code must retain the above copyright    <br>"
    "notice, this list of conditions and the following disclaimer.          <br>"
    "    * Redistributions in binary form must reproduce the above          <br>"
    "copyright notice, this list of conditions and the following disclaimer <br>"
    "in the documentation and / or other materials provided with the        <br>"
    "distribution.                                                          <br>"
    "    * Neither the name of Google Inc.nor the names of its              <br>"
    "contributors may be used to endorse or promote products derived from   <br>"
    "this software without specific prior written permission.               <br>"
    "                                                                       <br>"
    "THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    <br>"
    "\"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT    <br>"
    "LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  <br>"
    "A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT    <br>"
    "OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  <br>"
    "SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT        <br>"
    "LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  <br>"
    "DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  <br>"
    "THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    <br>"
    "(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  <br>"
    "OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.";

  emit addAboutInfo(info, "FilePTS");
}

//----------------------------------------------------------------

namespace SplatDataFileFormat
{
  // Standard Property Types (File Format)
  enum STDPropTypes
  {
    FLOAT = 0,
    FLOATVEC2 = 1,
    FLOATVEC3 = 2,
    DOUBLE = 3,
    DOUBLEVEC2 = 4,
    DOUBLEVEC3 = 5,
    INT32 = 6,
    INT32VEC2 = 7,
    INT32VEC3 = 8,
    INT16 = 9,
    INT16VEC2 = 10,
    INT16VEC3 = 11,
    INT8 = 12,
    INT8VEC2 = 13,
    INT8VEC3 = 14,
    UINT32 = 15,
    UINT32VEC2 = 16,
    UINT32VEC3 = 17,
    UINT16 = 18,
    UINT16VEC2 = 19,
    UINT16VEC3 = 20,
    UINT8 = 21,
    UINT8VEC2 = 22,
    UINT8VEC3 = 23,
    INT32ARRAY = 24,
    FLOATVEC2ARRAY = 25,
    FLOATVEC3ARRAY = 26,
  };
}


bool FilePTSPlugin::readBinaryFile( const char *_filename, SplatCloud &_splatCloud ) /*const*/
{
  // clear splatcloud
  _splatCloud.clear();

  // set default options
  bool loadPositions  = OpenFlipperSettings().value( "FilePTS/Load/Positions", true ).toBool();
  bool loadNormals    = OpenFlipperSettings().value( "FilePTS/Load/Normals", true ).toBool();
  bool loadPointsizes = OpenFlipperSettings().value( "FilePTS/Load/Pointsizes", false ).toBool();
  bool loadColors     = OpenFlipperSettings().value( "FilePTS/Load/Colors", false ).toBool();
//int  loadColorRange = 0;
  bool loadIndices    = OpenFlipperSettings().value( "FilePTS/Load/Indices", false ).toBool();

  // get options
  if( OpenFlipper::Options::gui() && loadOptions_ )
  {
    loadPositions  = loadOptions_->listWidget->findItems("Position",Qt::MatchExactly)[0]->isSelected();
    loadNormals    = loadOptions_->listWidget->findItems("Normal",Qt::MatchExactly)[0]->isSelected();
    loadPointsizes = loadOptions_->listWidget->findItems("PointSize",Qt::MatchExactly)[0]->isSelected();
    loadColors     = loadOptions_->listWidget->findItems("Color",Qt::MatchExactly)[0]->isSelected();
    loadIndices    = loadOptions_->listWidget->findItems("Index",Qt::MatchExactly)[0]->isSelected();
  }

  // request properties
  bool success = true;
                       { if( !_splatCloud.requestPositions()  ) success = false; }
  if( loadNormals    ) { if( !_splatCloud.requestNormals()    ) success = false; }
  if( loadPointsizes ) { if( !_splatCloud.requestPointsizes() ) success = false; }
  if( loadColors     ) { if( !_splatCloud.requestColors()     ) success = false; }
  if( loadIndices    ) { if( !_splatCloud.requestIndices()    ) success = false; }

  // check success of requests
  if( !success )
  {
    emit log( LOGERR, tr("Out of memory for input file \"%1\".\n").arg( _filename ) );
    return false; // return failure
  }

  // open file
  FILE *file = fopen( _filename, "rb" );
  if( !file )
  {
    emit log( LOGERR, tr("Could not open input file \"%1\".\n").arg( _filename ) );
    return false;
  }

  // read file type
  int fileType = 0;
  fread( &fileType, sizeof(int), 1, file );

  // check file type
  if( fileType != 1 && fileType != 2 && fileType != 3 )
  {
    emit log( LOGERR, tr("Bad filetype (%1) in input file \"%2\".\n").arg( QString::number( fileType ), _filename ) );
    fclose( file );
    return false; // return failure
  }

  // read number of splats
  unsigned int numSplats = 0;
  fread( &numSplats, sizeof(unsigned int), 1, file );

  // set number of splats
  _splatCloud.resizeSplats( numSplats );

  if (fileType < 3)
  {
      std::vector<ReadObject> readObject = getReadObjectOrder();

      for ( const ReadObject& element : readObject) {


          // read positions
          if ( element == PointPos)
          {
              unsigned int i;
              for (i = 0; i < numSplats; ++i)
              {
                  float pos[3];
                  fread(pos, sizeof(float), 3, file);

                  SplatCloud::Position position;
                  position[0] = pos[0];
                  position[1] = pos[1];
                  position[2] = pos[2];

                  _splatCloud.positions(i) = position;
              }
          }

          // read normals
          if ( element == PointNormal && loadNormals)
          {
              unsigned int i;
              for (i = 0; i < numSplats; ++i)
              {
                  float nrm[3];
                  fread(nrm, sizeof(float), 3, file);

                  SplatCloud::Normal normal;
                  normal[0] = nrm[0];
                  normal[1] = nrm[1];
                  normal[2] = nrm[2];

                  _splatCloud.normals(i) = normal;
              }
          }

          // read pointsizes
          if (element == PointSize && loadPointsizes)
          {
              unsigned int i;
              for (i = 0; i < numSplats; ++i)
              {
                  float ps = 0.0f;
                  fread(&ps, sizeof(float), 1, file);

                  SplatCloud::Pointsize pointsize;
                  pointsize = ps;

                  _splatCloud.pointsizes(i) = pointsize;
              }
          }

          // read colors
          if (element == PointColor && loadColors)
          {
              unsigned int i;
              for (i = 0; i < numSplats; ++i)
              {
                  unsigned int col = 0;
                  fread(&col, sizeof(unsigned int), 1, file);

                  SplatCloud::Color color; // ignore colorrange
                  color[0] = (unsigned char)((col >> 16) & 0xFF);
                  color[1] = (unsigned char)((col >> 8) & 0xFF);
                  color[2] = (unsigned char)((col) & 0xFF);

                  _splatCloud.colors(i) = color;
              }
          }

          // read indices
          if (element == PointIndex && loadIndices)
          {
              unsigned int i;
              for (i = 0; i < numSplats; ++i)
              {
                  int idx = -1;
                  fread(&idx, sizeof(idx), 1, file);

                  SplatCloud::Index index;
                  index = idx;

                  _splatCloud.indices(i) = index;
              }
          }

      }

  }
  else if (fileType == 3)
  {
    // file contains named property containers

    int numProperties = 0;
    fread(&numProperties, sizeof(int), 1, file);

    for (int propID = 0; propID < numProperties; ++propID)
    {
      // read property chunk

      // property name
      int propNameLen = 0;
      fread(&propNameLen, sizeof(int), 1, file);
      std::string propName(propNameLen, 0);
      if (propNameLen > 0)
        fread(&propName[0], 1, propNameLen, file);

      // property data type
      int dataType = 0;
      fread(&dataType, sizeof(int), 1, file);

      // size of compressed data chunk
      quint64 compressedSize = 0;
      fread(&compressedSize, sizeof(quint64), 1, file);
      size_t compressedSizeT = static_cast<size_t>(compressedSize);

      if (compressedSize)
      {
        // read data
        if (dataType == SplatDataFileFormat::FLOATVEC3)
        {
          if (propName == "Points")
            readCompressedBinaryChunk(file, compressedSizeT, reinterpret_cast<char*>(&_splatCloud.positions(0)));
          else if (propName == "Normals" && loadNormals)
            readCompressedBinaryChunk(file, compressedSizeT, reinterpret_cast<char*>(&_splatCloud.normals(0)));
          else
            fseek(file, static_cast<long>(compressedSizeT), SEEK_CUR);
        }
        else if (dataType == SplatDataFileFormat::FLOAT)
        {
          if (propName == "Radii" && loadPointsizes)
            readCompressedBinaryChunk(file, compressedSizeT, reinterpret_cast<char*>(&_splatCloud.pointsizes(0)));
          else
            fseek(file, static_cast<long>(compressedSizeT), SEEK_CUR);
        }
        else if (dataType == SplatDataFileFormat::UINT16)
        {
          fseek(file, static_cast<long>(compressedSizeT), SEEK_CUR);
        }
        else if (dataType == SplatDataFileFormat::UINT32)
        {
          if (propName == "Colors" && loadColors)
          {
            std::vector<ACG::Vec4uc> fileColors(numSplats);
            readCompressedBinaryChunk(file, compressedSizeT, reinterpret_cast<char*>(&fileColors[0]));

            for (uint i = 0; i < numSplats; ++i)
            {
              for (int k = 0; k < 3; ++k)
                _splatCloud.colors(i)[k] = fileColors[i][k];
            }
          }
          else
            fseek(file, static_cast<long>(compressedSizeT), SEEK_CUR);
        }
        else if (dataType == SplatDataFileFormat::INT32)
        {
          fseek(file, static_cast<long>(compressedSizeT), SEEK_CUR);
        }
        else
        {
          emit log(LOGWARN, tr("Unknown Property type. \"%1\".\n").arg(_filename));
          fseek(file, static_cast<long>(compressedSizeT), SEEK_CUR);
        }
      }
    }
  }



  // check for errors
  if( ferror( file ) )
  {
    emit log( LOGERR, tr("Could not read input file \"%1\".\n").arg( _filename ) );
    fclose( file );
    return false; // return failure
  }
  if( feof( file ) )
  {
    emit log( LOGERR, tr("Unexpected end in input file \"%1\".\n").arg( _filename ) );
    fclose( file );
    return false; // return failure
  }

  // close file
  fclose( file );

  // return success
  return true;
}


//----------------------------------------------------------------


std::vector<ReadObject> FilePTSPlugin::getReadObjectOrder() {

    std::vector<ReadObject> readObject;

    for ( auto i = 0 ; i < loadOptions_->listWidget->count() ; ++i ) {
        if ( loadOptions_->listWidget->item(i)->isSelected() ) {

            if (loadOptions_->listWidget->item(i)->text() == "Position" ) {
                readObject.push_back(PointPos);
            }

            if (loadOptions_->listWidget->item(i)->text() == "Normal" ) {
                readObject.push_back(PointNormal);
            }

            if (loadOptions_->listWidget->item(i)->text() == "Color" ) {
                readObject.push_back(PointColor);
            }

            if (loadOptions_->listWidget->item(i)->text() == "PointSize" ) {
                readObject.push_back(PointSize);
            }

            if (loadOptions_->listWidget->item(i)->text() == "Index" ) {
                readObject.push_back(PointIndex);
            }
        }
    }

    QString input_order;
    for ( const ReadObject& i : readObject) {

        if ( i == PointPos )
            input_order += " Position";
        if ( i == PointNormal )
            input_order += " Normal";
        if ( i == PointColor )
            input_order += " Color";
        if ( i == PointSize )
            input_order += " Pointsize";
        if ( i == PointIndex )
            input_order += " Index";
    }

    emit log(LOGINFO,"Reading data in this order:" + input_order);

    return readObject;
}

//----------------------------------------------------------------


bool FilePTSPlugin::readTextFile  ( const char *_filename, AdaptorBase& _adaptor ) {
    // clear Mesh
    _adaptor.clear();

    // set default options
    bool pointCount     = OpenFlipperSettings().value( "FilePTS/Load/PointCount", true ).toBool();
    bool loadNormals    = OpenFlipperSettings().value( "FilePTS/Load/Normals", true ).toBool();
    bool loadPointsizes = OpenFlipperSettings().value( "FilePTS/Load/Pointsizes", false ).toBool();
    bool loadColors     = OpenFlipperSettings().value( "FilePTS/Load/Colors", false ).toBool();
    int  loadColorRange = OpenFlipperSettings().value( "FilePTS/Load/ColorRange",0 ).toInt();
    bool loadIndices    = OpenFlipperSettings().value( "FilePTS/Load/Indices", false ).toBool();


    std::vector<ReadObject> readObject;
    if(loadOptions_)
        readObject = getReadObjectOrder();
    else // default values are to be used, however they dont save order. assume sane default order
    {
        if(OpenFlipperSettings().value( "FilePTS/Load/Positions", false ).toBool())
            readObject.emplace_back(PointPos);
        if(loadNormals)
            readObject.emplace_back(PointNormal);
        if(loadColors)
            readObject.emplace_back(PointColor);
        if(loadPointsizes)
            readObject.emplace_back(PointSize);
        if(loadIndices)
            readObject.emplace_back(PointIndex);
    }
    


    if ( readObject[0] != PointPos ) {
        emit log(LOGERR,"Position Attribute has to be first in the File!");
        return false;
    }



    // get options
    if( OpenFlipper::Options::gui() && loadOptions_ )
    {
        loadNormals    = loadOptions_->listWidget->findItems("Normal",Qt::MatchExactly)[0]->isSelected();
        loadPointsizes = loadOptions_->listWidget->findItems("PointSize",Qt::MatchExactly)[0]->isSelected();
        loadColors     = loadOptions_->listWidget->findItems("Color",Qt::MatchExactly)[0]->isSelected();
        loadIndices    = loadOptions_->listWidget->findItems("Index",Qt::MatchExactly)[0]->isSelected();
        pointCount     =  loadOptions_->pointCount->isChecked();
        loadColorRange =  loadOptions_->comboColor->currentIndex();

    }

    // request properties
    if( loadNormals    ) { _adaptor.request_vertex_normals();  }
    if( loadColors     ) { _adaptor.request_vertex_colors();   }
    if( loadPointsizes ) { _adaptor.request_point_sizes(); }
    if( loadIndices    ) { _adaptor.request_indices();   }


    QFile f(_filename);

    if (!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        emit log( LOGERR, tr("Could not open input file \"%1\".\n").arg( _filename ) );
        return false;
    }

    QTextStream in(&f);

    QString line = in.readLine();

    // We have a point count header. So parse it and allocate memory in advance
    if (pointCount) {
        bool ok;
        const int size = line.toInt(&ok);
        if ( ok ) {
            emit log( LOGINFO, tr("Point count in header: \"%1\"").arg( size ) );
            _adaptor.reserve(size);
        } else {
            emit log( LOGERR, tr("Failed to read point count header. Got \"%1\"").arg( line ) );
            return false;
        }
    }

    int currentPoint = 0;
    // Process all lines in file.
    while (!in.atEnd()) {

        if ( (currentPoint % 100000) == 0 )
            emit log( LOGINFO, tr("Reading point %1\n").arg( currentPoint ) );

        for ( const ReadObject& i : readObject) {


            // Read a position, which should be always here:
            if ( i == PointPos )
            {
                double pos[3];
                in >> pos[0] >> pos[1] >> pos[2];
                if ( in.status() == QTextStream::Ok ) {
                    _adaptor.add_point( TriMesh::Point(pos) );
                } else {
                    emit log( LOGERR, tr("Failed to read postion for point %1\n").arg( currentPoint ) );
                    break;
                }
            }

            // Read color, if it has been selected
            if( i == PointColor && loadColors )
            {
                if( loadColorRange == COLORRANGE_0_1 )
                {
                    ACG::Vec4f color(0.0,0.0,0.0,1.0);

                    in >> color[0] >> color[1] >> color[2];

                    if ( in.status() == QTextStream::Ok ) {
                        _adaptor.setColor(color);
                    } else {
                        emit log( LOGERR, tr("Failed to read color for point %1\n").arg( currentPoint ) );
                        break;
                    }

                } else { // loadColorRange == COLORRANGE_0_255

                    int col[3];

                    in >> col[0] >> col[1] >> col[2];


                    ACG::Vec3uc color;

                    color[0] = (unsigned char) col[0];
                    color[1] = (unsigned char) col[1];
                    color[2] = (unsigned char) col[2];


                    if ( in.status() == QTextStream::Ok ) {
                        _adaptor.setColor(color);
                    } else {
                        emit log( LOGERR, tr("Failed to read color for point %1\n").arg( currentPoint ) );
                        break;
                    }
                }
            }

            // read normal
            if( i == PointNormal && loadNormals )
            {
                double nrm[3];
                in >> nrm[0] >> nrm[1] >> nrm[2];

                if ( in.status() == QTextStream::Ok ) {
                    TriMesh::Normal normal;
                    normal[0] = nrm[0];
                    normal[1] = nrm[1];
                    normal[2] = nrm[2];

                    _adaptor.setNormal(normal);
                } else {
                    emit log( LOGERR, tr("Failed to read normal for point %1\n").arg( currentPoint ) );
                    break;
                }

            }

            // read pointsize
            if( i == PointSize && loadPointsizes )
            {
                float ps;

                in >> ps;

                if ( _adaptor.adaptorType() == DATA_SPLATCLOUD ) {
                    if ( in.status() == QTextStream::Ok ) {
                        _adaptor.setPointSize(ps);
                    } else {
                        emit log( LOGERR, tr("Failed to read point size for point %1\n").arg( currentPoint ) );
                        break;
                    }
                } else
                    emit log(LOGERR, "Pointsize not implemented for meshes");

            }

            // read index
            if( i == PointNormal && loadIndices )
            {
                int idx = -1;

                in >> idx;

                if ( _adaptor.adaptorType() == DATA_SPLATCLOUD ) {
                    if ( in.status() == QTextStream::Ok ) {
                        _adaptor.setIndex(idx);
                    } else {
                        emit log( LOGERR, tr("Failed to read index for point %1\n").arg( currentPoint ) );
                        break;
                    }
                } else
                    emit log(LOGERR, "Index not implemented for meshes, skipped");

            }

        }

        ++currentPoint;
        line = in.readLine();
        //std::cerr << line.toStdString() << std::endl;
    }

    if ( in.status() != QTextStream::Ok ) {
        emit log( LOGERR, tr("Input File Stream Status not ok! Points read: %1\n").arg( currentPoint ) );
    }

    // Sanity Check:
    if ( !in.atEnd() ) {
        emit log( LOGERR, tr("Not at end of file! Points read: %1\n").arg( currentPoint ) );
    }

    f.close();

    // return success
    return true;

}

//----------------------------------------------------------------


bool FilePTSPlugin::writeBinaryFile( const char *_filename, const SplatCloudNode *_splatCloudNode ) /*const*/
{
  // set default options
  bool saveNormals    = OpenFlipperSettings().value( "FilePTS/Save/Normals", true ).toBool();
  bool savePointsizes = OpenFlipperSettings().value( "FilePTS/Save/Pointsizes", false ).toBool();
  bool saveColors     = OpenFlipperSettings().value( "FilePTS/Save/Colors", false ).toBool();
//int  saveColorRange = 0;
  bool saveIndices    = OpenFlipperSettings().value( "FilePTS/Save/Indices", false ).toBool();

  // get options
  if( OpenFlipper::Options::gui() && saveOptions_ )
  {
    saveNormals    = saveNormals_->   isChecked();
    savePointsizes = savePointsizes_->isChecked();
    saveColors     = saveColors_->    isChecked();
//  saveColorRange = saveColorRange_->currentIndex();
    saveIndices    = saveIndices_->   isChecked();
  }

  // use default values instead of returning a failure

//// check availability
//if( (                  !_splatCloudNode->splatCloud().hasPositions() ) ||
//    (saveNormals    && !_splatCloudNode->splatCloud().hasNormals())  ) ||
//    (savePointsizes && !_splatCloudNode->splatCloud().hasPointsizes()) ||
//    (saveColors     && !_splatCloudNode->splatCloud().hasColors()    ) ||
//    (saveIndices    && !_splatCloudNode->splatCloud().hasIndices()   )
//{
//  emit log( LOGERR, tr("Desired properties not available for output file \"%1\".\n").arg( _filename ) );
//  return false; // return failure
//}

  // open file
  FILE *file = fopen( _filename, "wb" );
  if( !file )
  {
    emit log( LOGERR, tr("Could not open output file \"%1\".\n").arg( _filename ) );
    return false;
  }

  // write file type
  int fileType = 1;
  fwrite( &fileType, sizeof(int), 1, file );

  // write number of splats
  unsigned int numSplats = _splatCloudNode->splatCloud().numSplats();
  fwrite( &numSplats, sizeof(unsigned int), 1, file );

  // write positions
  {
    unsigned int i;
    for( i=0; i<numSplats; ++i )
    {
      const SplatCloud::Position &position = _splatCloudNode->getPosition( i );

      float pos[3];
      pos[0] = position[0];
      pos[1] = position[1];
      pos[2] = position[2];

      fwrite( pos, sizeof(float), 3, file );
    }
  }

  // write normals
  if( saveNormals )
  {
    unsigned int i;
    for( i=0; i<numSplats; ++i )
    {
      const SplatCloud::Normal &normal = _splatCloudNode->getNormal( i );

      float nrm[3];
      nrm[0] = normal[0];
      nrm[1] = normal[1];
      nrm[2] = normal[2];

      fwrite( nrm, sizeof(float), 3, file );
    }
  }

  // write pointsizes
  if( savePointsizes )
  {
    unsigned int i;
    for( i=0; i<numSplats; ++i )
    {
      const SplatCloud::Pointsize &pointsize = _splatCloudNode->getPointsize( i );

      float ps;
      ps = pointsize;

      fwrite( &ps, sizeof(float), 1, file );
    }
  }

  // write colors
  if( saveColors )
  {
    unsigned int i;
    for( i=0; i<numSplats; ++i )
    {
      const SplatCloud::Color &color = _splatCloudNode->getColor( i );

      unsigned int col; // ignore colorrange
      col = (0xFF << 24) | (color[0] << 16) | (color[1] << 8) | (color[2]);

      fwrite( &col, sizeof(unsigned int), 1, file );
    }
  }

  // write indices
  if( saveIndices )
  {
    unsigned int i;
    for( i=0; i<numSplats; ++i )
    {
      const SplatCloud::Index &index = _splatCloudNode->getIndex( i );

      int idx;
      idx = index;

      fwrite( &idx, sizeof(int), 1, file );
    }
  }

  // check for errors
  if( ferror( file ) )
  {
    emit log( LOGERR, tr("Could not write output file \"%1\".\n").arg( _filename ) );
    fclose( file );
    return false; // return failure
  }

  // close file
  fclose( file );

  // return success
  return true;
}


//----------------------------------------------------------------


bool FilePTSPlugin::writeTextFile( const char *_filename, const SplatCloudNode *_splatCloudNode ) /*const*/
{
  // set default options
  bool saveNormals    = OpenFlipperSettings().value( "FilePTS/Save/Normals", true ).toBool();
  bool savePointsizes = OpenFlipperSettings().value( "FilePTS/Save/Pointsizes", false ).toBool();
  bool saveColors     = OpenFlipperSettings().value( "FilePTS/Save/Colors", false ).toBool();
  int  saveColorRange = OpenFlipperSettings().value( "FilePTS/Save/ColorRange",0 ).toInt();
  bool saveIndices    = OpenFlipperSettings().value( "FilePTS/Save/Indices", false ).toBool();

  // get options
  if( OpenFlipper::Options::gui() && saveOptions_ )
  {
    saveNormals    = saveNormals_->   isChecked();
    savePointsizes = savePointsizes_->isChecked();
    saveColors     = saveColors_->    isChecked();
    saveColorRange = saveColorRange_->currentIndex();
    saveIndices    = saveIndices_->   isChecked();
  }

  // open file
  FILE *file = fopen( _filename, "wt" );
  if( !file )
  {
    emit log( LOGERR, tr("Could not open output file \"%1\".\n").arg( _filename ) );
    return false;
  }

  // for all splats...
  unsigned int i, numSplats = _splatCloudNode->splatCloud().numSplats();
  for( i=0; i<numSplats; ++i )
  {
    // write position
    {
      const SplatCloud::Position &position = _splatCloudNode->getPosition( i );

      float pos[3];
      pos[0] = position[0];
      pos[1] = position[1];
      pos[2] = position[2];

      fprintf( file, "%.6g %.6g %.6g", pos[0], pos[1], pos[2] );
    }

    // write color
    if( saveColors )
    {
      const SplatCloud::Color &color = _splatCloudNode->getColor( i );

      if( saveColorRange == COLORRANGE_0_1 )
      {
        static const float RCP255 = 1.0f / 255.0f;

        float col[3];
        col[0] = RCP255 * color[0];
        col[1] = RCP255 * color[1];
        col[2] = RCP255 * color[2];

        fprintf( file, " %.6g %.6g %.6g", col[0], col[1], col[2] );
      }
      else // saveColorRange == COLORRANGE_0_255
      {
        int col[3]; // use int, *not* unsigned char !
        col[0] = color[0];
        col[1] = color[1];
        col[2] = color[2];

        fprintf( file, " %i %i %i", col[0], col[1], col[2] );
      }
    }

    // write normal
    if( saveNormals )
    {
      const SplatCloud::Normal &normal = _splatCloudNode->getNormal( i );

      float nrm[3];
      nrm[0] = normal[0];
      nrm[1] = normal[1];
      nrm[2] = normal[2];

      fprintf( file, " %.6g %.6g %.6g", nrm[0], nrm[1], nrm[2] );
    }

    // write pointsize
    if( savePointsizes )
    {
      const SplatCloud::Pointsize &pointsize = _splatCloudNode->getPointsize( i );

      float ps;
      ps = pointsize;

      fprintf( file, " %.6g", ps );
    }

    // write index
    if( saveIndices )
    {
      const SplatCloud::Index &index = _splatCloudNode->getIndex( i );

      int idx;
      idx = index;

      fprintf( file, " %i", idx );
    }

    fprintf( file, "\n" );
  }

  // check for errors
  if( ferror( file ) )
  {
    emit log( LOGERR, tr("Could not write output file \"%1\".\n").arg( _filename ) );
    fclose( file );
    return false; // return failure
  }

  // close file
  fclose( file );

  // return success
  return true;
}


//----------------------------------------------------------------


bool FilePTSPlugin::readCompressedBinaryChunk(FILE* _file, size_t _compressedSize, char* _dst)
{
  std::vector<char> compressedData(_compressedSize);
  fread(&compressedData[0], 1, _compressedSize, _file);
  return snappy::RawUncompress(&compressedData[0], _compressedSize, _dst);
}

//----------------------------------------------------------------


int FilePTSPlugin::loadObject( QString _filename )
{
  // set default options
  bool loadBinaryFile = OpenFlipperSettings().value( "FilePTS/Load/BinaryFile", false ).toBool();
  enum dataType{ splatcloud,
                 trianglemesh,
                 polymesh } loadType = splatcloud;
  // get options
  if( OpenFlipper::Options::gui() && loadOptions_ )
  {
    if (loadOptions_->type->currentText() == "TriangleMesh") {
        loadType = trianglemesh;
    } else if (loadOptions_->type->currentText() == "PolyMesh") {
        loadType = polymesh;
    } else {
        loadType = splatcloud;
    }

    loadBinaryFile = loadOptions_->binary->isChecked();
  }


  if ( loadType == splatcloud ) {

      // add a new, empty splatcloud-object
      int splatCloudObjectId = -1;
      emit addEmptyObject( DATA_SPLATCLOUD, splatCloudObjectId );
      if( splatCloudObjectId != -1 )
      {
          // create list of ids and add id of splatcloud-object
          IdList objectIDs;
          objectIDs.push_back( splatCloudObjectId );

          // get splatcloud-object by id
          SplatCloudObject *splatCloudObject = 0;
          if( PluginFunctions::getObject( splatCloudObjectId, splatCloudObject ) )
          {
              // set name of splatcloud-object to filename
              splatCloudObject->setFromFileName( _filename );
              splatCloudObject->setName( splatCloudObject->filename() );

              // get splatcloud and scenegraph splatcloud-node
              SplatCloud     *splatCloud     = splatCloudObject->splatCloud();
              SplatCloudNode *splatCloudNode = splatCloudObject->splatCloudNode();
              if( (splatCloud != 0) && (splatCloudNode != 0) )
              {
                  AdaptorSplat adaptor(*splatCloud);

                  // read splatcloud from disk
                  if( loadBinaryFile ? readBinaryFile( _filename.toLatin1(), *splatCloud ) : readTextFile( _filename.toLatin1(), adaptor ) )
                  {
                      // emit signals that the object has to be updated and that a file was opened
                      emit updatedObject( splatCloudObjectId, UPDATE_ALL );
                      emit openedFile( splatCloudObjectId );

                      // get drawmodes
                      ACG::SceneGraph::DrawModes::DrawMode splatsDrawMode = ACG::SceneGraph::DrawModes::getDrawMode( "Splats" );
                      ACG::SceneGraph::DrawModes::DrawMode dotsDrawMode   = ACG::SceneGraph::DrawModes::getDrawMode( "Dots"   );
                      ACG::SceneGraph::DrawModes::DrawMode pointsDrawMode = ACG::SceneGraph::DrawModes::getDrawMode( "Points" );

                      // if drawmodes don't exist something went wrong
                      if( splatsDrawMode == ACG::SceneGraph::DrawModes::NONE ||
                              dotsDrawMode   == ACG::SceneGraph::DrawModes::NONE ||
                              pointsDrawMode == ACG::SceneGraph::DrawModes::NONE )
                      {
                          emit log( LOGERR, tr("Shader DrawModes for SplatCloud not existent!") );
                      }
                      else
                      {
                          // get global drawmode
                          ACG::SceneGraph::DrawModes::DrawMode drawmode = PluginFunctions::drawMode();

                          // if global drawmode does *not* contain any of 'Splats', 'Dots' or 'Points' drawmode, add 'Points'
                          if( !drawmode.containsAtomicDrawMode( splatsDrawMode ) &&
                                  !drawmode.containsAtomicDrawMode( dotsDrawMode   ) &&
                                  !drawmode.containsAtomicDrawMode( pointsDrawMode ) )
                          {
                              drawmode |= pointsDrawMode;
                              PluginFunctions::setDrawMode( drawmode );
                          }
                      }

                      // group objects
                      int groupObjectId = RPC::callFunctionValue<int>( "datacontrol", "groupObjects", objectIDs );
                      if( groupObjectId != -1 )
                      {
                          // everything is okay, so return id of group-object
                          return groupObjectId;
                      }
                  }
              }
          }

          // something went wrong, so delete objects
          size_t i, num = objectIDs.size();
          for( i=0; i<num; ++i )
              emit deleteObject( objectIDs[ i ] );
      }
  } else if ( loadType == trianglemesh ) {

      // add a new, empty splatcloud-object
      int triangleMeshID = -1;
      emit addEmptyObject( DATA_TRIANGLE_MESH, triangleMeshID );
      if( triangleMeshID != -1 )  {

          // get splatcloud-object by id
          TriMeshObject *triObject = 0;
          if( PluginFunctions::getObject( triangleMeshID, triObject ) )
          {
              // set name of splatcloud-object to filename
              triObject->setFromFileName( _filename );
              triObject->setName( triObject->filename() );

              // get splatcloud and scenegraph splatcloud-node
              TriMesh     *mesh     = triObject->mesh();

              if( mesh != 0 ) {
                  if ( loadBinaryFile ) {
                      emit log(LOGERR,"Binary not supported for mesh target!");
                  } else {
                      AdaptorMesh<TriMesh> adaptor(*mesh,DATA_TRIANGLE_MESH);

                      readTextFile( _filename.toLatin1(), adaptor ) ;

                      // emit signals that the object has to be updated and that a file was opened
                      emit updatedObject( triangleMeshID, UPDATE_ALL );
                      emit openedFile( triangleMeshID );

                      ACG::SceneGraph::DrawModes::DrawMode pointsDrawMode = ACG::SceneGraph::DrawModes::getDrawMode( "Points" );

                      triObject->meshNode()->drawMode(pointsDrawMode);
                      return triangleMeshID;
                  }
              }
          }
      }
  } else { // PolyMesh

      // add a new, empty splatcloud-object
      int polyMeshID = -1;
      emit addEmptyObject( DATA_POLY_MESH, polyMeshID );
      if( polyMeshID != -1 )  {

          // get splatcloud-object by id
          PolyMeshObject *triObject = 0;
          if( PluginFunctions::getObject( polyMeshID, triObject ) )
          {
              // set name of splatcloud-object to filename
              triObject->setFromFileName( _filename );
              triObject->setName( triObject->filename() );

              // get splatcloud and scenegraph splatcloud-node
              PolyMesh     *mesh     = triObject->mesh();

              if( mesh != 0 ) {
                  if ( loadBinaryFile ) {
                      emit log(LOGERR,"Binary not supported for mesh target!");
                  } else {
                      AdaptorMesh<PolyMesh> adaptor(*mesh,DATA_POLY_MESH);

                      readTextFile( _filename.toLatin1(), adaptor ) ;

                      // emit signals that the object has to be updated and that a file was opened
                      emit updatedObject( polyMeshID, UPDATE_ALL );
                      emit openedFile( polyMeshID );

                      ACG::SceneGraph::DrawModes::DrawMode pointsDrawMode = ACG::SceneGraph::DrawModes::getDrawMode( "Points" );

                      triObject->meshNode()->drawMode(pointsDrawMode);
                      return polyMeshID;
                  }
              }
          }
      }
  }

  // return failure
  return -1;
}


//----------------------------------------------------------------


bool FilePTSPlugin::saveObject( int _objectId, QString _filename )
{
  // set default options
  bool saveBinaryFile = OpenFlipperSettings().value( "FilePTS/Save/BinaryFile", false ).toBool();

  // get options
  if( OpenFlipper::Options::gui() && saveOptions_ )
  {
    saveBinaryFile = saveBinaryFile_->isChecked();
  }

  // get splatcloud-object by id
  SplatCloudObject *splatCloudObject = 0;
  if( PluginFunctions::getObject( _objectId, splatCloudObject ) )
  {
    // change name of splatcloud-object to filename
    splatCloudObject->setFromFileName( _filename );
    splatCloudObject->setName( splatCloudObject->filename() );

    // get splatcloud-node
    const SplatCloudNode *splatCloudNode = splatCloudObject->splatCloudNode();
    if( splatCloudNode != 0 )
    {
      // write splatcloud to disk
      if( saveBinaryFile ? writeBinaryFile( _filename.toLatin1(), splatCloudNode ) : writeTextFile( _filename.toLatin1(), splatCloudNode ) )
      {
        // return success
        return true;
      }
    }
  }

  // return failure
  return false;
}


//----------------------------------------------------------------


QWidget *FilePTSPlugin::loadOptionsWidget( QString /*_currentFilter*/ )
{
    if ( loadOptions_ == nullptr) {
        loadOptions_ = new ptsLoadWiget();

        loadOptions_->listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

        QStringList order = OpenFlipperSettings().value( "FilePTS/Load/Order", "position,normal,color,size,index"  ).toString().split(",");

        loadOptions_->listWidget->clear();

        for ( const QString& name : order ) {

            if ( name == "position" ) {
                loadOptions_->listWidget->addItem("Position");
            }

            if ( name == "normal" ) {
                loadOptions_->listWidget->addItem("Normal");
            }

            if ( name == "color" ) {
                loadOptions_->listWidget->addItem("Color");
            }

            if ( name == "size" ) {
                loadOptions_->listWidget->addItem("PointSize");
            }

            if ( name == "index" ) {
                loadOptions_->listWidget->addItem("Index");
            }

        }

        loadOptions_->type->setCurrentIndex  (  OpenFlipperSettings().value( "FilePTS/Load/LoadType", 0 ).toInt() );
        loadOptions_->binary->setChecked     ( OpenFlipperSettings().value( "FilePTS/Load/BinaryFile", true ).toBool() );
        loadOptions_->pointCount->setChecked ( OpenFlipperSettings().value( "FilePTS/Load/PointCount", true ).toBool() );
        loadOptions_->listWidget->findItems("Position",Qt::MatchExactly)[0]->setSelected( OpenFlipperSettings().value( "FilePTS/Load/Position",    true ).toBool() );
        loadOptions_->listWidget->findItems("Normal",Qt::MatchExactly)[0]->setSelected( OpenFlipperSettings().value( "FilePTS/Load/Normals",    true ).toBool() );
        loadOptions_->listWidget->findItems("PointSize",Qt::MatchExactly)[0]->setSelected(OpenFlipperSettings().value( "FilePTS/Load/Pointsizes", true ).toBool()) ;
        loadOptions_->comboColor->setCurrentIndex(OpenFlipperSettings().value( "FilePTS/Load/ColorRange",    0 ).toInt() );
        loadOptions_->listWidget->findItems("Color",Qt::MatchExactly)[0]->setSelected(OpenFlipperSettings().value( "FilePTS/Load/Colors",     true ).toBool())  ;
        loadOptions_->listWidget->findItems("Index",Qt::MatchExactly)[0]->setSelected(OpenFlipperSettings().value( "FilePTS/Load/Indices",    true ).toBool() );

        connect(loadOptions_->defaultButton,SIGNAL( clicked()  ) , this, SLOT(slotLoadMakeDefaultButtonClicked() ) );
        connect(loadOptions_->defaultButton,SIGNAL( clicked()  ) , this, SLOT(slotLoadMakeDefaultButtonClicked() ) );

    }

    return loadOptions_;
}


//----------------------------------------------------------------


QWidget *FilePTSPlugin::saveOptionsWidget( QString _currentFilter )
{
  if( saveOptions_ == 0 )
  {
    // create new widget (including Save Options and buttons)

    saveBinaryFile_ = new QCheckBox( tr("Save as Binary File") );

    saveNormals_    = new QCheckBox( tr("Save Normals")        );
    savePointsizes_ = new QCheckBox( tr("Save Pointsizes")     );
    saveColors_     = new QCheckBox( tr("Save Colors")         );

    saveColorRange_ = new QComboBox();
    saveColorRange_->addItem( "[0..1]"   );
    saveColorRange_->addItem( "[0..255]" );
    slotUpdateSaveColorRange();

    QHBoxLayout *saveColorsLayout = new QHBoxLayout();
    saveColorsLayout->setSpacing( 6 );
    saveColorsLayout->addWidget( saveColors_     );
    saveColorsLayout->addWidget( saveColorRange_ );

    saveIndices_ = new QCheckBox( tr("Save Indices") );

    QVBoxLayout *saveStructureLayout = new QVBoxLayout();
    saveStructureLayout->setSpacing( 6 );
    saveStructureLayout->addWidget( saveNormals_     );
    saveStructureLayout->addWidget( savePointsizes_  );
    saveStructureLayout->addItem  ( saveColorsLayout );
    saveStructureLayout->addWidget( saveIndices_     );

    QGroupBox *saveStructureGroupBox = new QGroupBox( tr("Internal File Structure") );
    saveStructureGroupBox->setLayout( saveStructureLayout );

    saveMakeDefaultButton_ = new QPushButton( tr("Make Default") );

    QVBoxLayout *saveLayout = new QVBoxLayout();
    saveLayout->setAlignment( Qt::AlignTop );
    saveLayout->setSpacing( 6 );
    saveLayout->addWidget( saveBinaryFile_        );
    saveLayout->addWidget( saveStructureGroupBox  );
    saveLayout->addWidget( saveMakeDefaultButton_ );

    saveOptions_ = new QWidget();
    saveOptions_->setLayout( saveLayout );

    // connect events to slots
    connect( saveBinaryFile_,        SIGNAL( stateChanged(int) ), this, SLOT( slotUpdateSaveColorRange()         ) );
    connect( saveColors_,            SIGNAL( stateChanged(int) ), this, SLOT( slotUpdateSaveColorRange()         ) );
    connect( saveMakeDefaultButton_, SIGNAL( clicked()         ), this, SLOT( slotSaveMakeDefaultButtonClicked() ) );

    // get Save Options from OpenFlipper (from disc)
    saveBinaryFile_->setChecked     ( OpenFlipperSettings().value( "FilePTS/Save/BinaryFile", true ).toBool() );
    saveNormals_->   setChecked     ( OpenFlipperSettings().value( "FilePTS/Save/Normals",    true ).toBool() );
    savePointsizes_->setChecked     ( OpenFlipperSettings().value( "FilePTS/Save/Pointsizes", true ).toBool() );
    saveColors_->    setChecked     ( OpenFlipperSettings().value( "FilePTS/Save/Colors",     true ).toBool() );
    saveColorRange_->setCurrentIndex( OpenFlipperSettings().value( "FilePTS/Save/ColorRange",    0 ).toInt()  );
    saveIndices_->   setChecked     ( OpenFlipperSettings().value( "FilePTS/Save/Indices",    true ).toBool() );
  }

  return saveOptions_;
}


//----------------------------------------------------------------

void FilePTSPlugin::slotUpdateSaveColorRange()
{
  saveColorRange_->setEnabled( saveColors_->isChecked() && !saveBinaryFile_->isChecked() );
}


//----------------------------------------------------------------


void FilePTSPlugin::slotLoadMakeDefaultButtonClicked()
{

  // pass our Load Options to OpenFlipper (to disc)    
  OpenFlipperSettings().setValue( "FilePTS/Load/LoadType",   loadOptions_->type->currentIndex()       );
  OpenFlipperSettings().setValue( "FilePTS/Load/PointCount", loadOptions_->pointCount->isChecked()  );
  OpenFlipperSettings().setValue( "FilePTS/Load/BinaryFile", loadOptions_->binary->isChecked()    );
  OpenFlipperSettings().setValue( "FilePTS/Load/Positions",    loadOptions_->listWidget->findItems("Position",Qt::MatchExactly)[0]->isSelected()    );
  OpenFlipperSettings().setValue( "FilePTS/Load/Normals",    loadOptions_->listWidget->findItems("Normal",Qt::MatchExactly)[0]->isSelected()    );
  OpenFlipperSettings().setValue( "FilePTS/Load/Pointsizes", loadOptions_->listWidget->findItems("PointSize",Qt::MatchExactly)[0]->isSelected()    );
  OpenFlipperSettings().setValue( "FilePTS/Load/Colors",     loadOptions_->listWidget->findItems("Color",Qt::MatchExactly)[0]->isSelected()    );
  OpenFlipperSettings().setValue( "FilePTS/Load/ColorRange", loadOptions_->comboColor->currentIndex() );
  OpenFlipperSettings().setValue( "FilePTS/Load/Indices",    loadOptions_->listWidget->findItems("Index",Qt::MatchExactly)[0]->isSelected()    );

  QString order = "";

  for ( auto i = 0 ; i < loadOptions_->listWidget->count() ; ++i ) {
      if (loadOptions_->listWidget->item(i)->text() == "Position" ) {
          order +="position,";
      }

      if (loadOptions_->listWidget->item(i)->text() == "Normal" ) {
          order +="normal,";
      }

      if (loadOptions_->listWidget->item(i)->text() == "Color" ) {
          order +="color,";
      }

      if (loadOptions_->listWidget->item(i)->text() == "PointSize" ) {
          order +="size,";
      }

      if (loadOptions_->listWidget->item(i)->text() == "Index" ) {
          order +="index,";
      }
  }

  // remove last ","
  order.chop(1);

  OpenFlipperSettings().setValue( "FilePTS/Load/Order",    order   );
}


//----------------------------------------------------------------


void FilePTSPlugin::slotSaveMakeDefaultButtonClicked()
{
  // pass our Save Options to OpenFlipper (to disc)
  OpenFlipperSettings().setValue( "FilePTS/Save/BinaryFile", saveBinaryFile_->isChecked()    );
  OpenFlipperSettings().setValue( "FilePTS/Save/Normals",    saveNormals_->   isChecked()    );
  OpenFlipperSettings().setValue( "FilePTS/Save/Pointsizes", savePointsizes_->isChecked()    );
  OpenFlipperSettings().setValue( "FilePTS/Save/Colors",     saveColors_->    isChecked()    );
  OpenFlipperSettings().setValue( "FilePTS/Save/ColorRange", saveColorRange_->currentIndex() );
  OpenFlipperSettings().setValue( "FilePTS/Save/Indices",    saveIndices_->   isChecked()    );
}


//================================================================

